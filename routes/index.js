import loginRoutes from "./loginRoutes.js";
import gameRoutes from "./gameRoutes.js";

export default app => {
  app.use("/login", loginRoutes);
  app.use("/game", gameRoutes);
};
