import {createElement} from "./helper.mjs";


export const createRacerElement = (user, isOwnElem) => {

    const roomElem = createElement({
        tagName: "div",
        className: "racer-item",
        attributes: {id: user.name}
    });

    const spanUserState = createElement({
        tagName: "span",
        className: `dot ${user.ready ? 'user-state-ready' : ''}`
    });

    const spanName = createElement({
        tagName: "span",
    });
    spanName.innerText = isOwnElem ? user.name + "(you)" : user.name;


    const progressBar = createElement({
        tagName: "div",
        className: "progress-bar-border",
    });
    progressBar.appendChild(createElement({
        tagName: "div",
        className: `progress-bar-container ${user.progress === 100 ? '.progress-bar-full ' : ''}`,
        attributes: {id: `${user.name}-progress-bar`}
    }));
    progressBar.style.width = user.progress

    roomElem.appendChild(spanUserState);
    roomElem.appendChild(spanName);
    roomElem.appendChild(progressBar);

    return roomElem


};
