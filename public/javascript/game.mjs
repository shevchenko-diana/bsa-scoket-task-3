import {addClass, createElement, initializeClock, removeClass} from "./helper.mjs";
import {createRacerElement} from "./raceElementCreators.js";
import {startRace} from "./race.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
    window.location.replace("/login");
}
const socket = io("http://localhost:3002/game", {query: {username}});

let removeEventListeners;
let gameInterval;
const roomContainer = document.getElementById("rooms-container");
const backToRoomsBtn = document.getElementById("back-btn");
const createRoomButton = document.getElementById("create-room-btn");
const createRoomPrompt = document.getElementById("create-room-prompt");
const submitRoomButton = document.getElementById("submit-room-btn");
const raceField = document.getElementById("race-field");


const onClickCreateRoomButton = () => {
    console.log("onClickCreateRoomButton")
    if (createRoomButton.innerText === "Cancel") {
        addClass(createRoomPrompt, "display-none")
        addClass(submitRoomButton, "display-none")
        createRoomButton.innerText = "Create Room"
    } else if (createRoomButton.innerText === "Create Room") {
        removeClass(createRoomPrompt, "display-none")
        removeClass(submitRoomButton, "display-none")
        createRoomButton.innerText = "Cancel"

    }
}

const onClickSubmitRoomButton = () => {
    if (createRoomPrompt.value === '') {
        return
    }
    socket.emit("CREATE_ROOM", createRoomPrompt.value);
    addClass(createRoomPrompt, "display-none")
    addClass(submitRoomButton, "display-none")
    createRoomButton.innerText = "Create Room"
    createRoomPrompt.value = ''
};

createRoomButton.addEventListener("click", onClickCreateRoomButton);
submitRoomButton.addEventListener("click", onClickSubmitRoomButton);
backToRoomsBtn.addEventListener("click", () => {

    socket.emit("LEAVE_ROOM",
        {
            roomId: document.getElementById("room-info-title").innerText,
            username
        })

});


const createRoomElement = room => {

    const roomElem = createElement({
        tagName: "div",
        className: "room-item",
        attributes: {id: room.id}
    });

    const spanUsers = createElement({
        tagName: "span",
    });
    spanUsers.innerText = `${room.users.length} users joined the room`;

    const spanRoomName = createElement({
        tagName: "h4",
    });
    spanRoomName.innerText = room.id;

    const btn = createElement({
        tagName: "a", className: "flex-centered"
    });
    btn.innerText = "Join";
    btn.addEventListener("click", () => socket.emit("JOIN_ROOM", room.id));

    roomElem.appendChild(spanUsers);
    roomElem.appendChild(spanRoomName);
    roomElem.appendChild(btn);

    return roomElem


};

const updateRooms = rooms => {
    roomContainer.innerHTML = '';
    rooms.map(room => roomContainer.appendChild(createRoomElement(room)))

};
const newUserForbidden = () => {
    alert('such nickname is already in use')
    window.location.replace("/login");
    sessionStorage.removeItem('username')
};
const createRoomForbidden = () => {
    alert('room with such title is already in use')
};


const initRace = (text, room) => {
    raceField.innerHTML = '';
    raceField.appendChild(createElement({
        tagName: "span",
        attributes: {id: 'game-timer-clock'}
    }))
    raceField.appendChild(createElement({tagName: 'br'}))
    console.log(room)

    removeEventListeners = startRace(username, text, updateUserProgress(room,
        room.users.filter(user => user.name === username)))

    gameInterval = initializeClock(
        'game-timer-clock',
        room.game.duration, () => {
            removeEventListeners()
        }
    )

}

const updateUserProgress = (room, user) => (progress) => {
    const state = {roomId: room.id, username, ready: user.ready, progress: progress,}
    if (progress === 100) {
        state.finishedAt = new Date().getTime()
    }
    socket.emit("USER_NEW_STATE", state)
}

const displayEndGameMessage = (room) => {
    const finishInfo = room.users.map(user => {
        return {
            name: user.name,
            finishedIn: (new Date(user.finishedAt).getTime() - new Date(room.game.startAt).getTime()) / 1000
        }
    })
    finishInfo.sort((a, b) => a.finishedIn < b.finishedIn ? -1 : 1)
    const msg = finishInfo.map(info => `user ${info.name} finished in ${info.finishedIn} seconds\n`)

    alert(msg)
}


async function fetchText(textId) {

    let response = await fetch(`/game/texts/${textId}`);

    if (response.status === 200) {
        return await response.json()
    }
}

const prepareRoomBeforeRace = room => {
    addClass(document.getElementById("rooms-page"), "display-none")
    removeClass(document.getElementById("game-page"), "display-none")

    racersContainer.innerHTML = '';
    raceField.innerHTML = '';
    const readyBtn = createElement({
        tagName: "button",
        attributes: {id: "ready-btn"}
    })
    roomInfoTitle.innerText = room.id;
    room.users.map(user => {
        const racer = createRacerElement(user, user.name === username)
        if (user.name === username) {
            readyBtn.innerText = user.ready ? "Not ready" : "Ready"
            readyBtn.addEventListener("click", () => {
                socket.emit("USER_NEW_STATE", {roomId: room.id, username, isReady: !user.ready})
            });
        }
        racersContainer.appendChild(racer)
    });
    raceField.appendChild(readyBtn)
}


async function joinRoom(room) {

    if (room.state === "ROOM_IN_PROGRESS") {
        //update bars
        let areAllFinished = true;
        room.users.map(user => {
            const userProgress = document.getElementById(`${user.name}-progress-bar`)
            userProgress.style.width = user.progress + '%';
            if (user.progress === 100) {
                addClass(userProgress, 'progress-bar-full')
            } else {
                areAllFinished = false
            }
        });

        if (areAllFinished || room.state === "ROOM_FINISHED") {
            displayEndGameMessage(room)
            document.getElementById('game-timer-clock').innerHTML = ''
            removeEventListeners()
            clearInterval(gameInterval)
            socket.emit("GAME_FINISHED", room.id)
            prepareRoomBeforeRace(room)

        }
        return

    }

    prepareRoomBeforeRace(room)


    if (room.state === "ROOM_READY") {
        socket.emit("ROOM_IN_PROGRESS", room.id)
        raceField.innerHTML = '';
        raceField.appendChild(createElement({
            tagName: "span",
            attributes: {id: 'start-timer-clock'}
        }))

        raceField.appendChild(createElement({tagName: 'br'}))

        removeClass(backToRoomsBtn, 'display-none')
        const text = await fetchText(room.game.textId)


        initializeClock(
            'start-timer-clock',
            room.game.startAt, () => {
                initRace(text.text, room)


            }
        )

    }


}

socket.on("UPDATE_ROOMS", updateRooms);
socket.on("CREATE_ROOM_DONE", joinRoom);
socket.on("JOIN_ROOM_DONE", joinRoom);
socket.on("NEW_USER_FORBIDDEN", newUserForbidden);
socket.on("CREATE_ROOM_FORBIDDEN", createRoomForbidden);
socket.on("UPDATE_ROOM_STATE", room => {
    if (roomInfoTitle.innerText === room.id) {
        joinRoom(room)
    }
});


const roomInfoTitle = document.getElementById("room-info-title");
const racersContainer = document.getElementById("racers-container");


