import {addClass, createElement} from "./helper.mjs";

let cursorPosition = -1;
let shiftPressed = false;
let text = ''

const onKeyUpClosure = (updateStatFn) => (ev) => {

    const nextChar = text[cursorPosition + 1]
    // console.log(`--${nextChar}--`, nextChar, 'cursorPosition', cursorPosition)
    if (nextChar === ev.key || (nextChar === ev.key.toUpperCase() && shiftPressed)) {
        cursorPosition += 1
        updateStatFn()
        // console.log('match')
    }
    if (ev.key === 'Shift') {
        shiftPressed = false
    }
};

const onKeyDown = (ev) => {
    if (ev.key === 'Shift') {
        shiftPressed = true
    }
};
const spanCompleted = createElement({tagName: 'span', className: 'span-race-completed'})
const spanCharNext = createElement({tagName: 'span', className: 'span-next-char'})
const spanLeftText = createElement({tagName: 'span', className: 'span-left-text'})
const progressBar = username => document.getElementById(`${username}-progress-bar`)

const updateRaceState = (username, updateProgressCb) => () => {
    console.log('updateTextSpans')

    spanCompleted.innerText = text.substr(0, cursorPosition + 1)

    spanCharNext.innerText = cursorPosition === text.length - 1 ? '' : text[cursorPosition + 1]

    spanLeftText.innerText = cursorPosition === text.length - 1 ? '' : text.substr(cursorPosition + 2,)
    const pb = progressBar(username)
    const width = (cursorPosition + 1) / text.length * 100
    pb.style.width = width + '%'
    if (width === 100) {
        addClass(pb, 'progress-bar-full')

    }
    updateProgressCb(width)


}

export const startRace = (username, textReceived, updateProgressCb) => {
    cursorPosition = -1;
    shiftPressed = false;
    text = ''
    text = textReceived

    const updateState = updateRaceState(username, updateProgressCb)
    let keyUp = onKeyUpClosure(updateState)

    window.addEventListener('keyup', keyUp)
    window.addEventListener('keydown', onKeyDown)
    updateState()

    const raceField = document.getElementById("race-field");

    raceField.appendChild(spanCompleted)
    raceField.appendChild(spanCharNext)
    raceField.appendChild(spanLeftText)

    return () => {
        window.removeEventListener('keyup', keyUp)
        window.addEventListener('keydown', onKeyDown)
    }


}
