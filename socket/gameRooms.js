import {ROOM_AVAILABLE, ROOM_BUSY, ROOM_FINISHED, ROOM_READY, Rooms, Users} from './data.js'
import {MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME} from "./config.js";
import {texts} from "../data.js";
import {ROOM_IN_PROGRESS} from "./data";

const createNotReadyUser = username => {
    return {name: username, ready: false}
};


const filterOutBusyRooms = () => {
    return Rooms.filter(room => room.state === ROOM_AVAILABLE)
};
export default io => {
    io.on("connection", socket => {
        const username = socket.handshake.query.username;
        // console.log("gameroom connected", username);

        //todo check username uniqueness

        socket.emit("UPDATE_ROOMS", filterOutBusyRooms());

        socket.on("NEW_USER", roomName => {
            if (Users.filter(user => user.name === username).length !== 0) {
                socket.emit("NEW_USER_FORBIDDEN")
                return
            }
            Users.push({name: username})

        })

        socket.on("LEAVE_ROOM", ({roomId, username}) => {
            // console.log("receive LEAVE_ROOM", roomId)
            const room = Rooms.filter(room => room.id === roomId)[0]
            const rIndex = Rooms.findIndex(room => room.id === roomId)
            if (rIndex === -1) {
                return
            }

            Rooms[rIndex].users = room.users.filter(user => user.name !== username);
            if (Rooms[rIndex].users.length < MAXIMUM_USERS_FOR_ONE_ROOM) {
                Rooms[rIndex].state = ROOM_AVAILABLE
            }
            Rooms[rIndex] = checkRoomState(room)
            console.log(room)

            if (Rooms[rIndex].users.length === 0) {
                Rooms.splice(rIndex, 1)
            }
            socket.emit("UPDATE_ROOMS", filterOutBusyRooms());
            socket.broadcast.emit("UPDATE_ROOMS", filterOutBusyRooms());
            socket.broadcast.to(roomId).emit("UPDATE_ROOM_STATE", Rooms.filter(room => room.id === roomId)[0])
            // io.sockets.in().emit("UPDATE_ROOM_STATE", Rooms.filter(room => room.id === roomId)[0])
            // io.emit("UPDATE_ROOMS", filterOutBusyRooms());

        })
        socket.on("CREATE_ROOM", roomName => {
            if (Rooms.filter(room => room.id === roomName).length !== 0) {
                socket.emit("CREATE_ROOM_FORBIDDEN")
                return
            }
            const newRoom = {
                id: roomName,
                name: roomName,
                state: ROOM_AVAILABLE,
                users: [createNotReadyUser(username)],
                game: {}
            };
            Rooms.push(newRoom);

            socket.join(roomName, () => {
                io.to(socket.id).emit("CREATE_ROOM_DONE", newRoom);
            });


            socket.emit("UPDATE_ROOMS", filterOutBusyRooms());
            socket.broadcast.emit("UPDATE_ROOMS", filterOutBusyRooms());
        });

        socket.on("ROOM_IN_PROGRESS", roomId => {
            const rIndex = Rooms.findIndex(room => room.id === roomId)
            Rooms[rIndex].state = "ROOM_IN_PROGRESS";
        })

        socket.on("GAME_FINISHED", roomId => {
            const room = Rooms.filter(room => room.id === roomId)[0];
            const rIndex = Rooms.findIndex(room => room.id === roomId)
            if (rIndex === -1) {
                return
            }
            Rooms[rIndex] = roomToDefaultState(room)
            console.log("GAME_FINISHED", room)

            socket.emit("UPDATE_ROOM_STATE", Rooms.filter(room => room.id === roomId)[0])
            socket.broadcast.emit("UPDATE_ROOM_STATE", Rooms.filter(room => room.id === roomId)[0])
            socket.broadcast.emit("UPDATE_ROOMS", filterOutBusyRooms());

        })

        socket.on("JOIN_ROOM", roomId => {


            const chosenRoom = Rooms.filter(room => room.id === roomId)[0];
            if (chosenRoom.users.length === MAXIMUM_USERS_FOR_ONE_ROOM ||
                chosenRoom.users.filter(user => user.name === username).length !== 0) {
                // socket.emit("JOIN_ROOM_FORBIDDEN")
                return
            }
            chosenRoom.users.push(createNotReadyUser(username))
            if (chosenRoom.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
                chosenRoom.state = ROOM_BUSY
            }
            //todo add check for username uniqueness again to avoid self competing
            const rIndex = Rooms.findIndex(room => room.id === roomId)
            Rooms[rIndex] = chosenRoom;

            socket.join(roomId, () => {
                socket.emit("JOIN_ROOM_DONE", Rooms.filter(room => room.id === roomId)[0]);
                socket.emit("UPDATE_ROOM_STATE", Rooms.filter(room => room.id === roomId)[0])
                socket.broadcast.emit("UPDATE_ROOM_STATE", Rooms.filter(room => room.id === roomId)[0])
                socket.broadcast.emit("UPDATE_ROOMS", filterOutBusyRooms());
            });

        });

        socket.on("USER_NEW_STATE", ({roomId, username, isReady, progress, finishedAt}) => {
            const room = Rooms.filter(room => room.id === roomId)[0]
            const rIndex = Rooms.findIndex(room => room.id === roomId)
            if (rIndex === -1) {
                return
            }
            // console.log(room, "in USER_NEW_STATE")
            const uIndex = room.users.findIndex(user => user.name === username)
            if (uIndex === -1) {
                return
            }
            room.users[uIndex].ready = isReady
            room.users[uIndex].progress = progress
            if (progress === 100) {
                room.users[uIndex].finishedAt = finishedAt
            }
            if (room.state === ROOM_IN_PROGRESS && room.game.duration < new Date().getTime()) {
                room.state = ROOM_FINISHED
                room.users = ROOM_FINISHED
            }

            Rooms[rIndex] = checkRoomState(room)

            socket.emit("UPDATE_ROOM_STATE", Rooms.filter(room => room.id === roomId)[0])
            socket.broadcast.emit("UPDATE_ROOM_STATE", Rooms.filter(room => room.id === roomId)[0])
        })

    });

};

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

const checkRoomState = room => {
    const everybodyReady = isEverybodyReady(room.users)
    if (everybodyReady && room.state !== ROOM_IN_PROGRESS) {
        room.state = ROOM_READY
        const startD = new Date(new Date().getTime() + SECONDS_TIMER_BEFORE_START_GAME * 1000);
        const endD = new Date(startD.getTime() + SECONDS_FOR_GAME * 1000);
        room.game = {
            startAt: startD,
            duration: endD,
            textId: getRandomInt(texts.length)
        }
    }
    return room
}

const isEverybodyReady = users => users.every(user => user.ready)
const isEverybodyFinished = users => users.every(user => user.progress === 100)

const roomToDefaultState = room => {

    room.state = ROOM_AVAILABLE
    room.game = {}

    room.users.forEach((user, uIndex) => {
        room.users[uIndex] = createNotReadyUser(user.name)
    })

    if (room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
        room.state = ROOM_BUSY
    }
    console.log('room in roomToDefaultState', room)
    return room
}
