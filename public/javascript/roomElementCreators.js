import {createElement} from "./helper.mjs";

export const createRoomElement = (socket, room) => {

    const roomElem = createElement({
        tagName: "div",
        className: "room-item",
        attributes: {id: room.id}
    });

    const spanUsers = createElement({
        tagName: "span",
    });
    spanUsers.innerText = `${room.users.length} users joined the room`;

    const spanRoomName = createElement({
        tagName: "span",
    });
    spanRoomName.innerText = room.id;

    const btn = createElement({
        tagName: "a", className: "flex-centered"
    });
    btn.innerText = "Join";
    btn.addEventListener("click", () => socket.emit("JOIN_ROOM", room.id));

    roomElem.appendChild(spanUsers);
    roomElem.appendChild(spanRoomName);
    roomElem.appendChild(btn);

    return roomElem


};
